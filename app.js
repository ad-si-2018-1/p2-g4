var express     = require('express'),         // módulo express
    app         = express(),                  // objeto express
    bodyParser  = require('body-parser'),     // processa corpo de requests
    cookieParser = require('cookie-parser'),  // processa cookies
    irc = require('irc');
	server = require('http').Server(app);
	socketio = require('socket.io')(server);
	socketio_cookieParser = require('socket.io-cookie');

app.use(bodyParser.json());
app.use(bodyParser.urlencoded( { extended: true } ));
app.use(cookieParser());
app.use(express.static('public'));
socketio.use(socketio_cookieParser);

var path = require('path');	// módulo usado para lidar com caminhos de arquivos

var proxies = {}; // mapa de proxys
var proxy_id = 0;

function proxy(id, servidor, nick, canal, socket ) {
	// var 	 = []; // cache de mensagens
	var ws = socket;

	var irc_client = new irc.Client(
			servidor, 
			nick,
			{channels: [canal],});

	irc_client.addListener('message'+canal, function (from, message) {
	    console.log(from + ' => '+ canal +': ' + message);
		/* cache.push(	{"timestamp":Date.now(), 
			"nick":from,
			"msg":message} );*/
	     ws.send(message);	
	});
	irc_client.addListener('error', function(message) {
	    console.log('error: ', message);
	});
	irc_client.addListener('mode', function(message) {
	    console.log('mode: ', message);
	});
	irc_client.addListener('motd', function(motd) {
	    console.log('motd: ', motd);
	    cache.push({"timestamp":Date.now(), 
			"nick":"Server Status",
			"msg": "<pre>"+motd+"</pre>" }
			);
	});
	// proxies[id] = { "cache":cache, "irc_client":irc_client  };
	proxies[id] = { "ws": ws, "irc_client":irc_client  };

	return proxies[id];
}

// a criação do web socket deve ocorrer depois do estabelecimento do login (cookie) 
/*
socketio.listen(server).on('connection', function (socket) {
    var id = socket.request.headers.cookie.id;
    //console.log("id: "+id);
    proxies[id].ws = socket;

    socket.on('message', function (msg) {
        console.log('Message Received: ', msg);
        // socket.broadcast.emit('message', msg);
	var irc_client = proxies[id].irc_client;
	irc_client.say(irc_client.opt.channels[0], msg );
    });
});
*/

app.get('/', function (req, res) {
  if ( req.cookies.servidor && req.cookies.nick  && req.cookies.canal ) {
	proxy_id++;
	// aqui deve ser configurado o socket
	socketio.listen(server).on('connection', function (socket) {
		socket.send("conectado");
		socket.on('message', function (msg) {
			console.log('Message Received: ', msg);
			var irc_client = proxies[id].irc_client;
			irc_client.say(irc_client.opt.channels[0], msg );
		});
		// o proxy deve ser criado após o estabelecimento da conexao
		var p =	proxy(	proxy_id,
				req.cookies.servidor,
				req.cookies.nick, 
				req.cookies.canal,
				socket);
	});
	res.cookie('id', proxy_id);
  	res.sendFile(path.join(__dirname, '/index.html'));
  }
  else {
        res.sendFile(path.join(__dirname, '/login.html'));
  }
});

/*
app.get('/obter_mensagem/:timestamp', function (req, res) {
  var id = req.cookies.id;
  res.append('Content-type', 'application/json');
  res.send(proxies[id].cache);
});
*/

app.post('/gravar_mensagem', function (req, res) {
  //proxies[req.cookies.id].cache.push(req.body);
  var irc_client = proxies[req.cookies.id].irc_client;
  if (req.body.msg == "/motd") {
	irc_client.send("motd");
  } else {
	  proxies[req.cookies.id].cache.push(req.body);
	  irc_client.say(irc_client.opt.channels[0], req.body.msg );
  }
  res.end();
});

app.get('/mode/:usuario/:args', function (req, res){
  var usuario = req.params.usuario;
  var args = req.params.args;
  //var retorno = '{"usuario":'+usuario+','+
  //		  '"args":"'+args+'}';
  
  var irc_client = proxies[req.cookies.id].irc_client;
  var retorno = irc_client.send("mode", usuario, args);
  res.send(retorno);
});
app.get('/mode/', function (req, res){
  
  var irc_client = proxies[req.cookies.id].irc_client;
  var retorno = irc_client.send("mode", req.cookies.nick);
  res.send(retorno);
});
app.get('/time/', function (req, res){
  var diasSemana = ['Domingo', 'Segunda', 'Terça', 'Quarta', 'Quinta', 'Sexta', 'Sábado'];
  var meses = ['Janeiro', 'Fevereiro', 'Março', 'Abril', 'Maio', 'Junho', 'Julho', 'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro'];

  var dataAtual = new Date();
  var dia = diasSemana[dataAtual.getDay()];
  var mes = meses[dataAtual.getMonth()];
	
  if (req.cookies.servidor) {
	res.send(req.cookies.servidor + ' ' + dia + ', ' + dataAtual.getDate() + ' de ' + mes + ' de ' + dataAtual.getFullYear() + '.');
  } else {
	res.send(dia + ', ' + dataAtual.getDate() + ' de ' + mes + ' de ' + dataAtual.getFullYear() + '.');
  }  
});


app.post('/login', function (req, res) { 
   res.cookie('nick', req.body.nome);
   res.cookie('canal', req.body.canal);
   res.cookie('servidor', req.body.servidor);
   res.redirect('/');
});

server.listen(3000, function () {
  console.log('Example app listening on port 3000!');	
});
